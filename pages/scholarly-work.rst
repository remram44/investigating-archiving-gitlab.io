.. title: Scholarly Work
.. slug: scholarly-work
.. date: 2019-12-01
.. description: Scholarly output from our Sloan grant.
.. type: text


.. publication_list:: bibtex/publications.bib
   :style: unsrt
   :detail_page_dir: