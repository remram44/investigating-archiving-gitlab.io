<!--
.. title: U.S. Sanctions Impact on the Git Community
.. slug: git-hosting-sanctions
.. date: 2019-07-30
.. author: IASGE Team
.. link: https://gitlab.com/investigating-archiving-git/investigating-archiving-git.gitlab.io/blob/master/updates/2019-07-30_git-for-everyone.html
.. description: The IASGE team responds to the blocking of git hosting platforms for some community members.
.. type: text
-->

<!DOCTYPE html>
<html>
<body>

<p>The IASGE team is deviating from our regularly scheduled program of research to talk about restrictions being placed on members of our community. And disclaimer, the opinions expressed here do not necessarily reflect those of NYU or the Sloan Foundation. On July 20, 2019, Shahin Sorkh, a computer engineering student and full-time developer, was trending on HackerNews, where someone had linked his <a href="https://shahinsorkh.ir/2019/07/20/how-is-it-like-to-be-a-dev-in-iran">blog post about what is it like to be a dev in Iran</a>. This post, previously hosted on GitHub pages, 404'd on July 25th when GitHub decided to comply with U.S. export control laws, implemented on "Specially Designated Nationals (SDNs) and other denied or blocked parties under U.S. and other applicable law [...] including prohibited end uses described in <a href="https://www.ecfr.gov/cgi-bin/text-idx?SID=ad384e1f1e017076f8c0136f322f0a4c&mc=true&node=pt15.2.744&rgn=div5">17 CFR 744</a>", as mentioned in the <a href="https://help.github.com/en/articles/github-and-trade-controls">Trade Controls page</a> on GitHub Help. Users with IP addresses originating in Crimea, Sudan, Cuba, Iran, North Korea, and Syria or whose payment history or other information linked to those locations were affected.</p>

<p>And when we say affected...we mean that users received emails from GitHub saying that their accounts were blocked. Little-to-no advance notice was given, and no option to back up their repositories.</p>

<p>We, the IASGE team, have chosen to write about this because restriction to members of the Git community&#8212;even when authorized by Federal Law&#8212;has far-reaching and chilling consequences for open source, open scholarship, and for the open exchange of information and ideas.</p>

<!-- TEASER_END -->

<h3>Restrictions Revealed</h3>
  
<p>The restrictions on GitHub came to light when users found that they were unable to access their private GitHub repositories, and could not access some common GitHub features for their public repos (e.g. deleting them). Hamed Saeedi's <a href="https://twitter.com/Hamed/status/1154268514074660864">July 25th tweet</a> announcing that his account had been blocked was the first indication for many that GitHub would use its <a href="https://help.github.com/en/articles/github-terms-of-service">Terms of Service</a> to restrict access to people in, or from, sanctioned countries. The ToS specifically states: "You may not use GitHub in violation of export control or sanctions laws of the United States or any other applicable jurisdiction. You may not use GitHub if you are or are working on behalf of a Specially Designated National (SDN) or a person subject to similar blocking or denied party prohibitions administered by a U.S. government agency." Further, a notification on GitHub's Trade Controls page states that "Users are responsible for ensuring that the content they develop and share on GitHub.com complies with the U.S. export control laws, including the EAR [U.S. Export Administration Regulations] and the U.S. International Traffic in Arms Regulations (ITAR)." This message is encapsulated in the banner across blocked users' GitHub homepages.</p>

    <figure class="center">
      <img src="/images/yellow-banner.png" alt="Screenshot from GitHub of the yellow banner across banned users' pages." style="width: 200%; height: 200%;">
      <figcaption>Screenshot from GitHub of the yellow banner across banned users' pages.</figcaption>
    </figure>
    <br/>

<p>Ostensibly, the move to deny access to users from certain countries resulted from GitHub, and its parent company Microsoft (currently <a href="https://www.nytimes.com/2019/07/12/technology/amazon-oracle-jedi-pentagon.html">competing with Amazon for a Department of Defense cloud hosting contract</a>), choosing when and how to comply with an export control law that affects various countries put under sanction by the U.S. government as well as SDNs. According to the <a href="https://help.github.com/en/articles/github-and-trade-controls">Trade Controls page</a> on GitHub's site, the service may allow users in sanctioned countries and territories access to certain free services as long as those users are not SDNs. According to the same page, GitHub explains the mechanism by which restrictions occur:</p>
  <blockquote>"If GitHub determines that a user or customer is located in a region that is subject to U.S. trade control restrictions, or a user is otherwise restricted under U.S. economic sanctions, then the affiliated account has been restricted to comply with those legal requirements. The determination of user and customer location to implement these legal restrictions are derived from a number of sources, including IP addresses and payment history. Nationality and ethnicity are not used to flag users for sanctions restrictions."</blockquote>

<p>Parsing this information is crucial because by GitHub's own policies it "[...] may allow users in or ordinarily resident in countries and territories subject to U.S. sanctions to access certain free GitHub.com services [...]" According to Saeedi's tweet, his blocked accounts were of this free variety, which raises questions regarding this current enforcement action and how blocks were rolled out. Even more questions arise from the case of Farzad YZ, a software engineer for <a href="https://www.futurice.com">Futurice</a>, who <a href="https://twitter.com/Farzad_YZ/status/1154640983377797120">tweeted that he lives and works in Finland</a> yet his account was restricted. He has filed an appeal but the process of vetting images of non-expired government-issued photo identification will take a while.</p>

<p>Meanwhile, other Git hosting platforms like GitLab, SourceForge, and Bitbucket are subject to the law (read their approaches: <a href="https://gitlab.com/gitlab-com/migration/issues/649">GitLab</a>, <a href="https://bitbucket.org/site/master/issues/9261/cant-access-site-form-iran">Bitbucket</a>, and <a href="https://www.computerworld.com/article/2467444/should-open-source-repositories-block-nations-under-u-s--sanctions-.html">SourceForge</a>) but have taken different approaches to enforcement. When GitLab moved its infrastructure from Microsoft Azure to Google Cloud Platform (GCP), Google informed them of the legal restrictions described above. GitLab chose not to disable any repositories immediately but rolled out a timeline that would allow users to migrate their repositories to a different platform. Rather than locking users out en masse with no warning, GitLab worked with the community to ensure that everyone could keep their data and voice their concerns. <a href="https://about.gitlab.com/2018/07/19/gcp-move-update">Their blog post</a> on the migration started with this note:</p>
  <blockquote>"NOTE to users in Crimea, Cuba, Iran, North Korea, Sudan, and Syria: GitLab.com may not be accessible after the migration to Google. Google has informed us that there are legal restrictions that are imposed for those countries. See this <a href="http://www.treasury.gov/resource-center/sanctions/Programs/Pages/Programs.aspx">U.S. Department of the Treasury link</a> for more details. At this time, we can only recommend that you download your code or export relevant projects as a backup. See <a href="https://gitlab.com/gitlab-com/migration/issues/649">this issue</a> for more discussion."</blockquote>

<p>So what can be done when the open source communities, valuing discovery, have invested in so much centralized infrastructure? Questions like this take on an even more urgent tone when we consider that this is not the first time that something like this has happened.</p>

<h3>This Has Happened Before</h3>
  
<p>On December 20, 2018, Slack banned all users from Cuba, Iran, Sudan, North Korea, Syria and the Crimea region of Ukraine in a manner similar to that used by GitHub: providing no prior warning about the enforcement of these laws and providing no mechanisms by which they can appeal the decision. The only avenue available to many was to turn to social media. Communication from Slack to the banned users simply said that their accounts were deactivated due to compliance with a U.S. embargo, stating that "in order to comply with export control and economic sanctions laws and regulations promulgated by the U.S. Department of Commerce and the U.S. Department of Treasury, Slack prohibits unauthorized use of its products and services in certain sanctioned countries [...]" GitHub's current denial of access also cites content and usage while also looking at geolocation of IPs and other information (e.g. payment history) linked to those six countries.</p>

<p>Oxford researcher Mahsa Alimardani spoke to <a href="https://web.archive.org/web/20190604194801/https://www.theverge.com/2018/12/20/18150129/slack-iran-deactivated-sanctions-license-cuba-crimea"><em>The Verge</em> about Slack's ban</a>, which she believes goes beyond the mandate of the law. According to Alimardani, "They [Slack] are either incompetent at OFAC interpretation or racist." On December 21st, Slack published a <a href="https://web.archive.org/web/20190728133106/https://slackhq.com/an-apology-and-an-update ">public apology and update</a> for their poor implementation, focusing on the lack of communication as a key issue. As part of their apology, the service noted that they would continue to block embargoed countries (to comply with the law) but would not deactivate user accounts. Individuals are able to access their accounts once they are no longer in sanctioned countries. </p>

<h3>Current State (as of 30 July)</h3>

<p>Emaad Ghorbani <a href="https://twitter.com/EmaadGH/status/1154310691559362560">initially reported</a> that his personal public GitHub account also blocked him from deleting his public repos. He noticed this after <a href="https://twitter.com/EmaadGH/status/1154262690359066627">he received an email from GitHub</a> (noreply@github.com) notifying him that his account "... has been restricted [with] limited access free GitHub repository services for personal communication only." These email notifications redirected restricted users to more details on <a href="https://help.github.com/en/articles/github-and-trade-controls">the GitHub and Trade Controls page</a> and a <a href="https://airtable.com/shrGBcceazKloz6pY">private appeal form hosted on Airtable</a>.</p>

<p>On July 27th, two days after users scrambled to understand the unexpected restrictions, GitHub CEO Nat Friedman finally replied on Twitter. Despite other hosting platforms communicating with their users before taking any action, Friedman asserted that "<a href="https://twitter.com/natfriedman/status/1155311124687945728">our understanding of the law does not give us the option to give anyone advance notice of restrictions</a>," as well as "<a href="https://twitter.com/natfriedman/status/1155311125967171585">We're not doing this because we want to; we're doing it because we have to. GitHub will continue to advocate vigorously with governments around the world for policies that protect software developers and the global open source community.</a>"</p>

<p>Soon following Friedman's Twitter thread, GitHub has been quietly rolling back some of their restrictions for users experiencing the block. Blocked users can choose to make their private repositories public and delete any repository under their account (these options did not exist prior), and GitHub pages are now building again. GitHub's lack of communication to their users at the beginning of this debacle, and continuing through it has left many to wonder if they'll ever be able to recover their repositories again. At the very least, GitHub's lack of communication has overly complicated an already bad situation and has raised questions about its ability to act as a fair keeper of other people's work.</p>
  
    <figure class="center">
      <img src="/images/phd-tweet.png" alt="Screenshot from Twitter of a PhD student asking GitHub for their dissertation code">
      <figcaption>Screenshot of <a href="">a tweet</a> from a PhD student asking GitHub for their dissertation code.</figcaption>
    </figure>
    <br/>

<p>The bottom line is: users from these six countries had no notice this was happening, and so couldn't transfer, make public or private, archive, or get their data out. Saeedi also reported in his <a href="https://medium.com/@hamed/yellow-badges-are-back-this-time-not-by-nazi-germany-not-for-jews-but-by-u-s-tech-companies-48e92d690176">latest blog update</a> on the situation that GitHub said that they were not legally able to send an export of disabled repositories (disclaimer: while we agree with these issues, we do not condone the use of symbols of Jewish oppression, such as the yellow badge, in this latest post). With no public-facing announcements, roadmaps, or advocacy campaigns planned, the most relevant questions still need answering:</p>

<ul>
  <li>Why did GitHub choose to do this now, and why do this silently?</li>
  <li>Which specific law or sanction required them to not give advance notice or let users export their data (and how come other companies did)?</li>
  <li>What is their plan to push back against this law they claim to not want to follow?</li>
</ul>
  
<h3>Why This Matters</h3>

<p>As librarians and researchers committed to openness and the preservation of the scholarly record that is distributed on these hosting platforms, we want to affirm our commitment to supporting scholars in the six affected countries (and globally!). We also want to say that we think the enforcement actions of GitHub are particularly problematic, especially when taken in the context of other hosting platforms' actions.</p>

<p>There's a widespread perception in certain circles of the academy that these hosting platforms are secure, but we see clearly here that some portions of our community are more susceptible to the effects of geopolitical instability than others. GitHub has never been, and will never be, the long-term access solution. Users with 10+ years of history on GitHub woke up one day locked out of their work on the 25th, confused and frustrated. We need more resilient infrastructure for scholarship.</p>

<p>For our IASGE project, we host the majority of our research on GitLab. What does this mean for Iranian or Cuban scholars or collaborators who would want to make a merge request on this post? It might be time for us to start looking at self-hosting our open source platform. In any case, IASGE is a grant-funded project wherein the first stage we are dedicated to researching how scholars use the features of platforms like GitHub and how we can preserve the repos for long-term, sustainable, and open accessibility. As <a href="https://twitter.com/muneeb/status/1155524704540737537">Muneedb Ali simply put it</a>, "Git protocol is already decentralized. All we need is decentralized login, storage, and social graph on top." We would like to see more opportunities to make use of and contribute to protocols like <a href="https://activitypub.rocks/">ActivityPub</a>. While there are only the beginning of Git-friendly platforms based on that protocol, there are several decentralized hosting platforms that supposedly offer the same scholarly outcomes like version control (obviously), community, collaboration, method tracking, publishing, education, reproducibility, continuous integration, and data management.</p>

<h3>Self-hosting Solutions</h3>
<p>For those who are startled or upset by these events and want to host their Git repositories on their own server (<a href="https://www.kimsufi.com/us/en/index.xml ">Kimsufi</a>, <a href="http://ionos.com/">Ionos</a>, and <a href="http://online.net/">Online.net</a> are potential places to rent servers outside the U.S.), here's a non-exhaustive list of open source hosting platforms that you could run on a minimal server (depending on your data and users, of course!):</p>

  <ul>
    <li><a href="https://gitlab.com/gitlab-org/gitlab-ce">GitLab Community Edition</a>: open core of GitLab's platform, Ruby-based</li>
    <li><a href="https://gogs.io/">Gogs</a>: Written in Go and small enough that you can run it on a Raspberry Pi!</li>
    <li><a href="https://gitea.io/en-us/">Gitea</a>: a community-managed fork of Gogs</li>
    <li><a href="https://www.phacility.com/phabricator/">Phabricator</a>: PHP based, supports a lot of features</li>
    <li><a href="https://sourcehut.org/">sourcehut</a>: Written in Python with no JavaScript, still in Alpha (so be careful w.r.t stability!)</li>
    <li><a href="https://blog.printf.net/articles/2015/05/29/announcing-gittorrent-a-decentralized-github/">GitTorrent</a>: "decentralized GitHub"</li>
    <li><a href="http://gitchain.org/">GitChain</a> & <a href="https://sit.fyi/">SIT</a>: Git data & associated activity on the blockchain</li>
    <li><a href="https://git.scuttlebot.io/%25n92DiQh7ietE%2BR%2BX%2FI403LQoyf2DtR3WQfCkDKlheQU%3D.sha256">Git-SSB</a>: Git on secure-scuttlebutt, a decentralized Git hosting system</li>
  </ul>

</body>
</html>